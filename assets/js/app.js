$(document).ready(function () {
    var url = 'ajax_delete_image';
    var busy = false;


    var loader = function(action) {
        busy = action;
    };

    var delete_image  = function ($btn) {
        if(!busy) {
            loader(true);
            $.ajax({
                url: url,
                type: 'GET',
                data: {
                    image_id: $btn.data('icode'),
                },
                success: function (result) {
                    if (result === "Success") {
                        console.log('result: ', result);
                    }
                    loader(false);
                    $btn.closest('.container__image').remove();
                    // location.reload();
                },
                error: function () {
                    loader(false);
                }
            });
        }

    };

    var $delete_image_btn = $('.js-delete-image');
    $delete_image_btn.each(function (i, el) {
        var $cur_btn = $(el);
        $cur_btn.click(function () {
            delete_dialog($cur_btn);
        })
    });


    var delete_dialog = function (icode) {
        $.confirm({
            type: "blue",
            typeAnimated: true,
            draggable: true,
            theme: "Light",
            title: "Удаление",
            content: "Вы действительно хотите удалить картинку?",
            escapeKey: 'cancel',
            boxWidth: '30%',
            useBootstrap: false,
            closeAnimation: 'zoom',
            backgroundDismiss: 'cancel',
            buttons: {
                yes: {
                    theme: "Dark",
                    text: "Да",
                    btnClass: "btn-primary btn--light",
                    action: function () {
                        delete_image(icode);
                    }
                },
                cancel: {
                    text: "Отмена",
                    btnClass: "btn-primary btn--light",
                }
            }
        })
    }
});