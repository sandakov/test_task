from datetime import datetime

from django.core.files.images import ImageFile
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase
from django.urls import reverse

from catalog.models import Image
from catalog.utils import get_image_file


class ImageListTest(TestCase):
    small_gif = (
        b'\x47\x49\x46\x38\x39\x61\x01\x00\x01\x00\x00\x00\x00\x21\xf9\x04'
        b'\x01\x0a\x00\x01\x00\x2c\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02'
        b'\x02\x4c\x01\x00\x3b')

    def setUp(self):
        img = Image.objects.create(
            image=ImageFile(open('media/catalog/avatar2.jpg', 'rb')),
            name='test_image1',
            created_at=datetime.now()
        ).save()
        Image.objects.create(
            image=SimpleUploadedFile(name='test_image2.jpg', content=self.small_gif, content_type='image/jpeg'),
            name='test_image2',
            created_at=datetime.now()
        ).save()

    def test_url_exist_and_have_correct_url(self):
        resp = self.client.get('/detail/1/')
        self.assertEqual(resp.status_code, 200)

    def test_image_detail_uses_correct_template(self):
        resp = self.client.get(reverse('home'))
        self.assertEqual(resp.status_code, 200)

        self.assertTemplateUsed(resp, 'catalog/image_list.html')