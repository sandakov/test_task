from datetime import datetime

from django.test import TestCase

from catalog.models import Image


class ImageModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        Image.objects.create(
            image='catalog/avatar2.jpg',
            name='TestImageName',
            created_at=datetime.now()
        )

    def test_name_label(self):
        image = Image.objects.get(id=1)
        field_label = image._meta.get_field('name').verbose_name
        self.assertEquals(field_label, 'Имя изображения')

    def test_max_length_image_name_field(self):
        image = Image.objects.get(id=1)
        max_length = image._meta.get_field('name').max_length
        self.assertEquals(max_length, 200)