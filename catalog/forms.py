from django import forms

from catalog.models import Image


class ImageUpdateForm(forms.ModelForm):

    class Meta:
        model = Image
        fields = ('image', 'name', 'created_at')


