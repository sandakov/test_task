from datetime import datetime
import os

from django.db import models
from PIL import Image as PilImage
import piexif
from django.db.models.signals import post_save
from django.dispatch import receiver


class Image(models.Model):

    image = models.ImageField(
        verbose_name='Картинка',
        upload_to='catalog/'
    )

    name = models.CharField(
        verbose_name='Имя изображения',
        max_length=200,
        blank=True,
        null=True,
    )

    created_at = models.DateTimeField(
        verbose_name='Дата создания',
        blank=True,
        null=True
    )

    class Meta:
        verbose_name = verbose_name_plural = 'Каталог изображений'

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        super().save(force_insert, force_update, using, update_fields)
        self.created_at = datetime.fromtimestamp(
            os.path.getctime(self.image.path)
        )
        # Необходимо сохранить еще раз, т.к. при первом сохрании файла в файловой структуре
        # не существует.
        # self.save(update_fields=['created_at'])

# @receiver(post_save, sender=Image, dispatch_uid='proceed_fields')
# def proceed_fields(sender, instance, **kwargs):
#     count = Image.objects.filter(name__icontains=instance.image).count()
#     instance.created_at = datetime.fromtimestamp(
#         os.path.getctime(instance.image.path)
#     )
#
#     if count > 0:
#         instance.name = f'{instance.image.path}_{count + 1}'

    # post_save.disconnect(proceed_fields, sender=Image)
    # instance.save()
    # post_save.connect(proceed_fields, sender=Image)

    # image = piexif.load(instance.image.path)


