import os
from datetime import datetime

from django.http import HttpResponse
from django.urls import reverse_lazy
from django.views.generic import ListView, UpdateView, FormView

from catalog.forms import ImageUpdateForm
from catalog.models import Image
from PIL import Image as PilImage


class BaseImageViewList(ListView):
    template_name = 'catalog/image_list.html'


class ImageViewList(BaseImageViewList):

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(object_list=object_list, **kwargs)
        context['title'] = f'Главная'
        return context

    def get_queryset(self):
        return Image.objects.all()


class SearchImageListView(BaseImageViewList):

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(object_list=object_list, **kwargs)
        query = self.kwargs['query']
        context['title'] = f'Поиск: {query}'
        return context

    def get(self, request, *args, **kwargs):
        if 'query' in request.GET:
            self.kwargs['query'] = request.GET.get('query')
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        if 'query' in self.kwargs:
            return Image.objects.filter(name__icontains=self.kwargs['query'])


def ajax_delete_image(request):
    if request.is_ajax():
        idx = request.GET.get('image_id')
        image = Image.objects.get(id=idx)
        os.remove(image.image.path)
        image.delete()
        return HttpResponse("Success")


class ImageUpdateView(UpdateView):
    form_class = ImageUpdateForm
    template_name = 'catalog/image_detail.html'
    success_url = reverse_lazy('home')

    def get_object(self, queryset=None):
        if 'idx' in self.kwargs:
            return Image.objects.get(id=self.kwargs['idx'])


class AddImageView(FormView):
    form_class = ImageUpdateForm
    template_name = 'catalog/image_detail.html'
    success_url = reverse_lazy('home')

    def get_context_data(self, **kwargs):
        context = super(AddImageView, self).get_context_data(**kwargs)
        context['add_flag'] = True
        return context

    def form_valid(self, form):
        instance = form.save(commit=False)
        count = Image.objects.filter(name__icontains=instance.image.file).count()
        filename = f'{instance.image.file}'

        if count:
            filename, ext = filename.split('.')
            filename += f'_{count + 1}.{ext}'
        instance.name = filename
        instance.created_at = datetime.now()
        instance.save()

        return super().form_valid(form)




