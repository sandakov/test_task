from django.contrib import admin

from catalog.models import Image


@admin.register(Image)
class ImageAdmin(admin.ModelAdmin):
    list_display = ('name', 'image', )
    list_display_links = ('name', )
