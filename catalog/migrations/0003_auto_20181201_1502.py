# Generated by Django 2.1.3 on 2018-12-01 10:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0002_auto_20181201_1439'),
    ]

    operations = [
        migrations.AddField(
            model_name='image',
            name='created_at',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Дата создания'),
        ),
        migrations.AddField(
            model_name='image',
            name='name',
            field=models.TextField(blank=True, max_length=200, null=True, verbose_name='Имя изображения'),
        ),
    ]
