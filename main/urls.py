from django.conf.urls import url
from django.contrib import admin
from django.conf.urls.static import static
from django.urls import path
from django.conf import settings

from catalog.views import (
    ImageViewList,
    ajax_delete_image,
    SearchImageListView,
    ImageUpdateView,
    AddImageView
)

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^$', ImageViewList.as_view(), name='home'),
    url(r'ajax_delete_image', ajax_delete_image, name='ajax_delete_image'),
    url(r'search/$', SearchImageListView.as_view(), name='search'),
    url(r'detail/(?P<idx>\d+)/$', ImageUpdateView.as_view(), name='detail'),
    url(r'add/$', AddImageView.as_view(), name='add')
] \
        + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) \
        + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
